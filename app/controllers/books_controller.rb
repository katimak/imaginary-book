# frozen_string_literal: true

class BooksController < ApplicationController
  def index
    @books = Book.all # get books from db
    render 'special_book_list'
  end
end
