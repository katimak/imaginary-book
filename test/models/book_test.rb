# frozen_string_literal: true

require 'test_helper'

class BookTest < ActiveSupport::TestCase
  def setup
    @a = Book.new
  end

  test 'name' do
    @a.name = 'abc'

    assert_equal @a.name, 'abc'
  end
  test 'cost' do
    @a.cost = 14.65

    assert @a.cost == 14.65
  end
end
