# frozen_string_literal: true

require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  test 'the truth' do
    a = Article.new(title: 'abc')

    assert a.title == 'abc'
  end
end
